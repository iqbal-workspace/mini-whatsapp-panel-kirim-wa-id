<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class CredentialController extends Controller
{
    public function index()
    {
        $data = DB::table('credential')->get();

        //dd($data);
        return view('credential.index',compact('data'));
    }

    public function create()
    {
        return view('credential.create');
    }

    public function post(Request $request)
    {
        DB::table('credential')->insert([
            'key_credential'        => $request->cred,
            'created_at'        => Carbon::now('Asia/Jakarta')
        ]);

        return back();
    }

    public function edit($id)
    {
        $ids = \Crypt::decrypt($id);
        $data = DB::table('credential')->where('id',$ids)->first();

        return view('credential.edit',compact('data'));
    }

    public function update($id, Request $request)
    {
        $ids = \Crypt::decrypt($id);
        DB::table('credential')->where('id',$ids)->update([
            'key_credential'                => $request->cred,
            'updated_at'                => Carbon::now('Asia/Jakarta')
        ]);

        return back();
    }

    public function delete($id)
    {
        $ids = \Crypt::decrypt($id);
        DB::table('credential')->where('id',$ids)->delete();

        return back();
    }
}
