<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DeviceController extends Controller
{
    public function index()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'devices',[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ]
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response, true);
        $data = $response_json;

        //dd($data);
        return view('device.index',compact('data'));
    }

    public function pairing($deviceID)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'qr?device_id='.$deviceID,[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ]
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response, true);
        $data = $response_json['image_url'];
        
        //dd($data);

        return view('device.pairing',compact('data'));
    }
}
