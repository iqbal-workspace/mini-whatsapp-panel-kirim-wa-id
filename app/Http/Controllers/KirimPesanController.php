<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;

class KirimPesanController extends Controller
{
    public function composePersonal()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'devices',[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ]
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response, true);
        $data = $response_json;

        return view('message.composePersonal', compact('data'));
    }

    public function sendComposePersonal(Request $requestForm)
    {
         if($requestForm->send_at == 'now')
         {
             $sendAt = 'now';
         }else{
             $datetime = new DateTime($requestForm->schedule);
             $sendAt = $datetime->format(DateTime::ATOM);
         }
 
         $dataUpload = [
                 'phone_number'          => $requestForm->phone_number,
                 'message'               => $requestForm->message,
                 'device_id'             => $requestForm->device_id,
                 'message_type'          => $requestForm->message_type,
                 'send_at'               => $sendAt,
             ];
 
         $headers = [
             'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA"),
             'Content-Type'          => 'application/json',
         ];
 
         $client = new \GuzzleHttp\Client();
         $request = $client->post(env("BASE_URL_KIRIM_WA").'messages',[
             'headers' => $headers,
             'json' => $dataUpload,
         ]);
 
         return back();
    }

    public function compose($groupID,$deviceID,$phoneNumber)
    {
        return view('message.compose',compact('groupID','deviceID','phoneNumber'));
    }

    public function sendCompose($groupID,$deviceID,$phoneNumber, Request $requestForm)
    {
        //dd($requestForm->all());
        if($requestForm->send_at == 'now')
        {
            $sendAt = 'now';
        }else{
            $datetime = new DateTime($requestForm->schedule);
            $sendAt = $datetime->format(DateTime::ATOM);
        }

        //dd($sendAt);

        $dataUpload = [
                'phone_number'          => $requestForm->phone_number,
                'message'               => $requestForm->message,
                'device_id'             => $requestForm->device_id,
                'message_type'          => $requestForm->message_type,
                'send_at'               => $sendAt,
            ];

        $headers = [
            'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA"),
            'Content-Type'          => 'application/json',
        ];
        //dd($dataUpload);

        $client = new \GuzzleHttp\Client();
        $request = $client->post(env("BASE_URL_KIRIM_WA").'messages',[
            'headers' => $headers,
            'json' => $dataUpload,
        ]);

        //$code = $request->getStatusCode();

        //dd($request);
        return back();
    }

    public function index($deviceID)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'groups?device_id='.$deviceID,[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ]
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response, true);
        $data = $response_json;
        $param = $deviceID;

        //dd($data);

        return view('message.index',compact('data','param'));
    }

    public function listMember($groupID,$deviceID)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'groups/'.$groupID.'?device_id='.$deviceID,[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ]
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response, true);
        $data = $response_json;

        //dd($data);

        return view('message.memberGroup',compact('data','groupID','deviceID'));
    }

    public function riwayatPengiriman()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'messages',[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ]
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response, true);
        $data = $response_json;

        //dd($data);

        return view('message.history',compact('data'));
    }
}
