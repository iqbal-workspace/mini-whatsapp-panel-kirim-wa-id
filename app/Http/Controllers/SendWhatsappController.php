<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;

class SendWhatsappController extends Controller
{

    public function otentikasi()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'?access_token='.env("ACCESS_TOKEN_KIRIM_WA"));
        
        $response = $request->getBody()->getContents();
        $response_json = json_decode($response);
        return response()->json($response_json, 200);
    }

    public function getDeviceListed()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'devices',[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ]
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response);
        return response()->json($response_json, 200);
    }

    public function addNewDevice()
    {
        $client = new \GuzzleHttp\Client();
        //insert data using json format
        $request = $client->post(env("BASE_URL_KIRIM_WA").'devices',[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ],
            'json'    => [
                'device_id'             => 'samsung-testing-device'
            ] 
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response);
        return response()->json($response_json, 200);
    }

    public function detailDevice($deviceID)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'devices/'.$deviceID,[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ]
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response);
        return response()->json($response_json, 200);
    }

    public function qrDevice($deviceID)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'qr?device_id='.$deviceID,[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ]
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response);
        return response()->json($response_json, 200);
    }

    public function listGroup($deviceID)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(env("BASE_URL_KIRIM_WA").'groups?device_id='.$deviceID,[
            'headers' => [
                'Authorization'         => 'Bearer '.env("ACCESS_TOKEN_KIRIM_WA")
            ]
        ]);

        $response = $request->getBody()->getContents();
        $response_json = json_decode($response);
        return response()->json($response_json, 200);
    }
}
