@extends('layouts.app-dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Credential Key
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('credential.index') }}"><i class="fa fa-key"></i> Credential Key</a></li>
        <li class="active">Edit Key</li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Credential</h3>
                    </div>
                    <form method="POST" action="{{ route('credential.update', Crypt::encrypt($data->id)) }}">
                        @csrf
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Credential Key</label>
                                <input type="text" value="{{ $data->key_credential }}" class="form-control" name="cred" placeholder="Enter credential key">
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </section>
</div>
@endsection