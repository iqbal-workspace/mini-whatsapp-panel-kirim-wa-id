@extends('layouts.app-dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Credential Key
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Credential Key</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <a href="{{ route('credential.create') }}" class="btn btn-sm btn-primary pull-right">Add Credential</a>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">No</th>
                  <th>Credential</th>
                  <th>CreatedAt</th>
                  <th>Action</th>
                </tr>
                @php $no=1; @endphp
                @foreach($data as $cred)
                <tr>
                    <td>{{$no++}}</td>
                    <td><input type="password" class="form-control" value="{{$cred->key_credential}}" id="keyCredential" name="keyCredential"></td>
                    <td>{{$cred->created_at}}</td>
                    <td>
                        <button class="btn btn-xs btn-primary" id="showHideButton" onclick="showHideKey(this);" data-toggle="tooltip" data-placement="top" title="Show & hide key"><i class="fa fa-eye"></i></button>
                        <a href="{{ route('credential.edit', Crypt::encrypt($cred->id)) }}" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
                        <a href="{{ route('credential.delete', Crypt::encrypt($cred->id)) }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach
               
               
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
</div>
<script>
function showHideKey() {
  var x = document.getElementById("keyCredential");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
</script>
@endsection