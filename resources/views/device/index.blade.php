@extends('layouts.app-dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Device
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">List of Device</li>
      </ol>
    </section>
    <hr>
    <div class="pad margin no-print">
      <div class="callout callout-info" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> Note:</h4>
        This page displays a list of devices that have been registered on the KirimWa.ID platform system.
        KirimWA.id is a free service to start chatting on WhatsApp without having to save the number to your contacts first.
        Click <a href="https://kirimwa.id/" target="blank">here</a> to know more.
      </div>
    </div>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">No</th>
                  <th>Device ID</th>
                  <th>Status</th>
                  <th>CreatedAt</th>
                  <th>Action</th>
                </tr>
                @php $no=1; @endphp
                @foreach ($data['data'] as $device)
                  <tr>
                      <td>{{$no++}}</td>
                      <td>{{ $device['id'] }}</td>
                      <td>
                          @if($device['status'] == "disconnected")
                              <a class="btn btn-xs btn-danger">OFF / Inactive</a>
                          @else
                            <a class="btn btn-xs btn-success">ON / Active</a>
                          @endif
                      </td>
                      <td>{{ $device['created_at'] }}</td>
                      <td>
                        <a href="{{ route('device.pairing', $device['id']) }}" class="btn btn-xs btn-warning"><i class="fa fa-fw fa-power-off"></i> | Pairing</a>
                        @if($device['status'] <> "disconnected")
                          <a href="{{ route('message.index', $device['id']) }}" class="btn btn-xs btn-primary"><i class="fa fa-envelope"></i> | Inbox</a>
                        @endif
                      </td>
                  </tr>
                @endforeach
               
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
</div>
@endsection