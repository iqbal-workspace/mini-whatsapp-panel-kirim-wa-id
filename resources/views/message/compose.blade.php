@extends('layouts.app-dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Whatsapp Mini Panel
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('device.index') }}"><i class="fa fa-fw fa-phone-square"></i> Device</a></li>
        <li><a href="{{ route('message.index',$deviceID) }}"><i class="fa fa-fw fa-phone-square"></i> {{$deviceID}}</a></li>
        <li><a href="{{ route('message.listMember',[$groupID,$deviceID]) }}"><i class="fa fa-users"></i> Group : {{$groupID}}</a></li>
        <li class="active">Compose Message</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        @include('message.sidebar-message')
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Compose new Message</h3>
              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Mail">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <!-- / box body --> 
            <form method="POST" action="{{ route('message.listMemberComposeSend',[$groupID,$deviceID,$phoneNumber]) }}" enctype="multipart/form-data">
                        @csrf
            <input type="hidden" class="form-control" name="device_id" value="{{$deviceID}}">
            <div class="box-body">
              <div class="form-group">
                <input class="form-control" name="phone_number" placeholder="To:" value="{{$phoneNumber}}" readonly="true">
              </div>
              <div class="form-group">
                <select class="form-control" id="tipePesan" name="message_type" onchange="toggleTipePesan()">
                      <option value="text">Please choose message type</option>
                      <option value="text">Text</option>
                      <option value="image">Image</option>
                      <option value="document">Document</option>
                </select>
              </div>
              <div id="infoPopup" style="display: none;" class="callout callout-info">
                <h4>Important !</h4>
                <p>Please input link / URL below.</p>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" placeholder="Type your message here....." rows="10"></textarea>
              </div>
              <div class="form-group">
                <select class="form-control" id="jadwalKirimPesan" name="send_at" onchange="toggleJadwalKirimPesan()">
                      <option value="now">Do you want to setup message schedule ?</option>
                      <option value="later">Yes</option>
                      <option value="now">No, i want to send message right now</option>
                </select>
              </div>
              <div id="infoPopup2" style="display: none;" class="callout callout-info">
                <h4>Important !</h4>
                <p>Please input send message schedule.</p>
              </div>
              <div class="form-group" style="display:none;" id="waktuKirimPesan">
                <input class="form-control" value="Please set schedule" name="schedule" type="datetime-local">
              </div>
            </div>
            <!-- / end box body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
              </div>
              <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
            </div>
            </form>
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script>
  function toggleTipePesan() {
    const el = document.getElementById('tipePesan');
    const box = document.getElementById('infoPopup');

    if (el.value !== 'text') {
      box.style.display = "block";
    } else {
      box.style.display = "none";
    }
  }

  function toggleJadwalKirimPesan() {
    const el2 = document.getElementById('jadwalKirimPesan');
    const box2 = document.getElementById('waktuKirimPesan');
    const box3 = document.getElementById('infoPopup2');

    if (el2.value === 'later') {
      box2.style.display = "block";
      box3.style.display = "block";
    } else {
      box2.style.display = "none";
      box3.style.display = "none";
    }
  }

</script>
@endsection