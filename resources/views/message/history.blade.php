@extends('layouts.app-dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Whatsapp Mini Panel
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('device.index') }}"><i class="fa fa-fw fa-phone-square"></i> Device</a></li>
        <li class="active">Whatsapp</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      @include('message.sidebar-message')
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">List Message History</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Mail">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                    @php $no=1; @endphp
                  @foreach($data['data'] as $hst)
                    <tr>
                        <td></td>
                        <td class="mailbox-star"></td>
                        <td class="mailbox-name"><a href="">Receiver : {{$hst['payload']['phone_number']}}</a></td>
                        <td class="mailbox-subject">Sender : {{ $hst['payload']['device_id'] }}
                        </td>
                        <td class="mailbox-attachment"></td>
                        <td class="mailbox-date">{{ date('d-m-Y H:i:s', strtotime($hst['created_at'])) }}</td>
                    </tr>
                    <tr>
                       <td colspan="6">
                            <p style="font-size : 10px;"><b>Message Body :</b><br> {{$hst['payload']['message']}}</p>
                       </td> 
                    </tr>
                    @endforeach
                  
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection
