@extends('layouts.app-dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Whatsapp Mini Panel
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('device.index') }}"><i class="fa fa-fw fa-phone-square"></i> Device</a></li>
        <li class="active">Whatsapp</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      @include('message.sidebar-message')
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">List Group</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Mail">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
        
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Refresh List Group</button>
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                    @php $no=1; @endphp
                  @foreach($data['data'] as $group)
                    <tr>
                        <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></td>
                        <td class="mailbox-star">{{$no++}}</td>
                        <td class="mailbox-name"><a href="">{{$group['name']}}</a></td>
                        <td class="mailbox-subject">
                            <a href="{{ route('message.listMember',[$group['id'],$param]) }}" class="btn btn-xs btn-primary"><i class="fa fa-users"></i> List Member</a>
                        </td>
                        <td class="mailbox-attachment"></td>
                        <td class="mailbox-date">{{$group['id']}}</td>
                    </tr>
                    @endforeach
                  
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Refresh List Group</button>
              </div>
            </div>
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection
