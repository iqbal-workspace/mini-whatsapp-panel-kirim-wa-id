@extends('layouts.app-dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Whatsapp Mini Panel
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('device.index') }}"><i class="fa fa-fw fa-phone-square"></i> Device</a></li>
        <li><a href="{{ route('message.index',$deviceID) }}"><i class="fa fa-fw fa-phone-square"></i> {{$deviceID}}</a></li>
        <li><a href="{{ route('message.listMember',[$groupID,$deviceID]) }}"><i class="fa fa-users"></i> Group : {{$groupID}}</a></li>
        <li class="active">List Member Group</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      @include('message.sidebar-message')
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">List Members</h3>
              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Mail">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                @foreach($data['data'] as $member)
                    <div class="col-md-3">
                        <!-- Profile Image -->
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <img class="profile-user-img img-responsive img-circle" src="{{ url('template/dist/img/user4-128x128.jpg') }}" alt="User profile picture">
                                <p class="text-muted text-center">{{$member}}</p>
                                <a href="{{ route('message.listMemberCompose',[$groupID,$deviceID,$member]) }}" class="btn btn-primary btn-block"><b>Send Message</b></a>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                   </div>
                @endforeach
                </div>
                <!-- / row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection