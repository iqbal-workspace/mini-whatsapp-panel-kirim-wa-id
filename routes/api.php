<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/authentication', [App\Http\Controllers\SendWhatsappController::class, 'otentikasi'])->name('send.wa.otentikasi');
Route::get('/list-device', [App\Http\Controllers\SendWhatsappController::class, 'getDeviceListed'])->name('send.wa.listPerangkat');
Route::post('/add-new-device', [App\Http\Controllers\SendWhatsappController::class, 'addNewDevice'])->name('send.wa.tambahPerangkat');
Route::get('/list-device/{deviceID}', [App\Http\Controllers\SendWhatsappController::class, 'detailDevice'])->name('send.wa.detailPerangkat');
Route::get('/list-device/qr/{deviceID}', [App\Http\Controllers\SendWhatsappController::class, 'qrDevice'])->name('send.wa.qrPerangkat');
Route::get('/list-device/groups/{deviceID}', [App\Http\Controllers\SendWhatsappController::class, 'listGroup'])->name('send.wa.grupPerangkat');