<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/device', [App\Http\Controllers\DeviceController::class, 'index'])->name('device.index');
    Route::get('/device/pairing/{deviceID}', [App\Http\Controllers\DeviceController::class, 'pairing'])->name('device.pairing');

    Route::get('/message/{deviceID}', [App\Http\Controllers\KirimPesanController::class, 'index'])->name('message.index');
    Route::get('/message/list-member/{groupID}/{deviceID}', [App\Http\Controllers\KirimPesanController::class, 'listMember'])->name('message.listMember');
    Route::get('/message/list-member/compose/{groupID}/{deviceID}/{phoneNumber}', [App\Http\Controllers\KirimPesanController::class, 'compose'])->name('message.listMemberCompose');
    Route::post('/message/list-member/compose/send/{groupID}/{deviceID}/{phoneNumber}', [App\Http\Controllers\KirimPesanController::class, 'sendCompose'])->name('message.listMemberComposeSend');
    Route::get('/message-sent-history', [App\Http\Controllers\KirimPesanController::class, 'riwayatPengiriman'])->name('message.riwayatPengiriman');
    Route::get('/message/personal/compose', [App\Http\Controllers\KirimPesanController::class, 'composePersonal'])->name('message.composePersonal');
    Route::post('/message/personal/compose/send', [App\Http\Controllers\KirimPesanController::class, 'sendComposePersonal'])->name('message.sendComposePersonal');

    Route::get('/setting/credential', [App\Http\Controllers\CredentialController::class, 'index'])->name('credential.index');
    Route::get('/setting/credential/add', [App\Http\Controllers\CredentialController::class, 'create'])->name('credential.create');
    Route::post('/setting/credential/post', [App\Http\Controllers\CredentialController::class, 'post'])->name('credential.post');
    Route::get('/setting/credential/edit/{id}', [App\Http\Controllers\CredentialController::class, 'edit'])->name('credential.edit');
    Route::post('/setting/credential/update/{id}', [App\Http\Controllers\CredentialController::class, 'update'])->name('credential.update');
    Route::get('/setting/credential/delete/{id}', [App\Http\Controllers\CredentialController::class, 'delete'])->name('credential.delete');

});